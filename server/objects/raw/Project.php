<?php

class Project extends DatabaseObject {
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 * @Getter
	 */
	private $id;
	
	/**
	 * @ColumnType("text")
	 * @Getter
	 * @Setter
	 */
	private $name;
	
	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $ownerID;
	
	/**
	 * @ColumnType("int")
	 * @Getter
	 * @Setter
	 */
	private $permission;

	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $creationDate;

	// @inject
}