<?php

class Settings {

	const COOKIE_NAME = "Cookie4Cassie";

	const DatabasePrefix = "Cassie_";

	const SESSION_EXPIRE = 6000;
	private static $executionTime = false;
	private static $dataSource = false;

	public static function getDatasource() {
		if (Settings::$dataSource === false) {
			Settings::$dataSource = new DatabaseDatasource ();
		}
		return Settings::$dataSource;
	}

	public static function getTime() {
		if (Settings::$executionTime === false) {
			Settings::$executionTime = time ();
		}
		return Settings::$executionTime;
	}

	const MYSQL_IP = "127.0.0.1";

	const MYSQL_USERNAME = "root";

	const MYSQL_PASSWORD = "123456";

	const MYSQL_DATABASE = "Cassandra";
	private static $ardata = array ();

	public static function clearHtmlComplete() {
		Settings::$ardata ["body"] = new HTML_BODY ();
		Settings::$ardata ["head"] = new HTML_HEAD ();
		Settings::$ardata ["html"] = new HTML_HTML ();
		Settings::$ardata ["title"] = new HTML_TITLE ();
	}

	public static function getBody() {
		if (! isset ( Settings::$ardata ["body"] )) {
			Settings::$ardata ["body"] = new HTML_BODY ();
		}
		return Settings::$ardata ["body"];
	}

	public static function getHtml() {
		if (! isset ( Settings::$ardata ["html"] )) {
			Settings::$ardata ["html"] = new HTML_HTML ();
		}
		return Settings::$ardata ["html"];
	}

	public static function getHead() {
		if (! isset ( Settings::$ardata ["head"] )) {
			Settings::$ardata ["head"] = new HTML_HEAD ();
		}
		return Settings::$ardata ["head"];
	}

	public static function getTitle() {
		if (! isset ( Settings::$ardata ["title"] )) {
			Settings::$ardata ["title"] = new HTML_TITLE ();
		}
		return Settings::$ardata ["title"];
	}

	public function setTitle($title) {
		Settings::getTitle ()->setTitle ( $title );
	}
	private static $cwd;

	public static function detectCWD() {
		static::$cwd = getcwd ();
	}

	public static function restoreCWD() {
		chdir ( static::$cwd );
	}

}

Settings::detectCwd ();