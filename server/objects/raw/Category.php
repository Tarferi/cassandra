<?php

class Category extends DatabaseObject {
	/**
	 * @ColumnType("int")
	 * @Primary
	 * @Getter
	 */
	private $id;
	
	/**
	 * @ColumnType("int");
	 * @Getter
	 * @Setter
	 */
	private $categoryName;
	
	/**
	 * @ColumnType("int")
	 * @Getter
	 * @Setter
	 */
	private $permission;

	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $creationDate;
	
	// @inject
}