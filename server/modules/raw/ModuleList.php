<?php

class ModuleList extends Module {

	public static function getModules($user) {
		$classes = get_declared_classes ();
		$items = array ();
		foreach ( $classes as $class ) {
			if (is_subclass_of ( $class, "Module" )) {
				$rc = new ReflectionClass ( $class );
				$item = $rc->newInstance ( array () );
				if ($user->hasPermission ( $item->getPermission () )) {
					$items [] = $item;
				}
			}
		}
		return $items;
	}

	public function getModuleData() {
		$body = Settings::getBody ();
		Settings::getTitle ()->setTitle ( "Cassandra module" );
		$body->addElement ( new HTML_CENTER ( new TitleBlockPanel ( "Module", new NameText ( "This module doesn't product any output!\nWe're sorry." ) ) ) );
		return true;
	}
	
	// @inject
}