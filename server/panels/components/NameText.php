<?php

class NameText extends HTML_SPAN {
	private $txt;

	public function __construct($text) {
		parent::__construct ();
		$txt = new HTML_TEXT ( $text );
		$this->setFontFamily ( "Verdana" );
		$this->setFontSize ( "12pt" );
		$this->addElement($txt);
	}

}