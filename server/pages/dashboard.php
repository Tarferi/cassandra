<?php
include_once "common.php";
requireLogin ();
Settings::getTitle ()->setTitle ( "Cassandra Dashboard" );
$modules = ModuleList::getModules ( Session::get ()->getUser () );
$body = Settings::getBody ();
$panel = new TitleBlockPanel ( "Available Modules" );
$panel->setTitleBackground ( "#00ddff" );
$hp = new horizontalPanel ( true );
foreach ( $modules as $module ) {
	$p = new ModuleListItemPanel ( $module );
	$hp->addComponent ( $p );
	$p->setName ( "module_" . $module->getClassName () );
	$p->addClickServerHandler ( function ($module) {
		redirect ( "module.php?module=" . $module->getClassName () );
		die ();
	}, $module );
}
$hp->setpadding ( "50px" );
$panel->addComponent ( $hp );
$body->addElement ( new HTML_CENTER ( $panel ) );
