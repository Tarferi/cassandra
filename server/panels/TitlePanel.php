<?php

class TitlePanel extends HTML_DIV {

	public function __construct($title) {
		parent::__construct ();
		$this->construct ( $title );
	}

	public function setTitle($title) {
		$this->tel->setText($title);
	}

	private function construct($title) {
		$this->tel = new HTML_TEXT ( $title );
		$this->addElement ( $this->tel );
		$this->setBackground ( "#00ffff" );
		$this->setFontFamily ( "Verdana" );
		$this->setFontSize("21pt");
		$this->setPadding("5px");
	}

}