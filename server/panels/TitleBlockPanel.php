<?php

class TitleBlockPanel extends HTML_DIV {
	private $title;
	private $contents;

	public function getTitleElement() {
		return $this->title;
	}

	public function setTitleBackground($color) {
		$this->title->setBackground ( $color );
	}

	public function __construct($title = false, $contents = false) {
		parent::__construct ();
		$this->title = new TitlePanel ( "" );
		$this->title->setBorder ( "1px", "solid" );
		$this->title->setHeight ( "22px" );
		$this->title->setFontSize ( "16pt" );
		$this->title->setBackground ( "#ff4400" );
		$this->contents = new HTML_DIV ();
		$this->contents->setBorderLeft ( "1px", "solid" );
		$this->contents->setBorderRight ( "1px", "solid" );
		$this->contents->setBorderBottom ( "1px", "solid" );
		$this->addElement ( new HTML_CENTER ( $this->title ) );
		$this->addElement ( $this->contents );
		if ($title !== false) {
			$this->title->setTitle ( $title );
		}
		if ($contents !== false) {
			$this->contents->addElement ( $contents );
		}
		$this->setDisplay ( "inline-block" );
	}

	public function setTitle($title) {
		$this->title->setTitle ( $title );
	}

	public function addComponent($contents) {
		$this->contents->addElement ( $contents );
	}

}