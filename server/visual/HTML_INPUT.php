<?php

class HTML_INPUT extends HTML_SINGLE_ELEMENT {

	public function __construct($type, $name = false, $id = false) {
		parent::__construct ( "input" );
		$this->addAttribute ( "type", $type );
		if ($name !== false) {
			$this->getRepresentationObject ()->addAttribute ( "name", $name );
		}
		if ($id !== false) {
			$this->getRepresentationObject ()->addAttribute ( "id", $id );
		}
	}

	public function setValue($value) {
		parent::getRepresentationObject ()->addAttribute ( "value", $value );
	}

}