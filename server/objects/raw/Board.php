<?php
class Board extends DatabaseObject {
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 * @Getter
	 */
	private $id;
	/**
	 * @ColumnType("text")
	 * @Getter
	 * @Setter
	 */
	private $boardName;
	/**
	 * @ColumnType("int")
	 * @Getter
	 * @Setter
	 */
	private $boardPermission;

	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $creationDate;

	// @inject
}