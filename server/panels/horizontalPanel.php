<?php

class horizontalPanel extends HTML_DIV {
	private $elements = 0;
	private $separator = false;

	public function __construct($separator = false) {
		parent::__construct ();
		$this->separator = $separator;
		$this->construct ();
	}

	private function construct() {
		$this->setDisplay("inline-block");
	}

	public function addComponent($object) {
		$this->elements ++;
		if ($this->separator) {
			if ($this->elements > 1) {
				$this->addElement ( new HTML_BR () );
				$this->addElement ( new HTML_HR () );
			}
		}
		$this->addElement ( $object );
	}

}