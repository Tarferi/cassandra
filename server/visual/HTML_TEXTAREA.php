<?php

class HTML_TEXTAREA extends HTML_PAIR_ELEMENT {
	private $content;

	public function __construct() {
		parent::__construct ( "textarea" );
		$this->content = new HTML_TEXT ();
	}

	public function getContent() {
		return $this->content->getText();
	}
	
	public function setContent($text) {
		$this->content->setText ( $text );
	}

}