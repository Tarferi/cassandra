<?php

class BlockPanel extends HTML_DIV {

	public function __construct($content = false) {
		parent::__construct ();
		$this->construct ();
		if ($content !== false) {
			$this->addElement ( $content );
		}
	}
	public function addContents($contents) {
		$this->addElement ( $contents );
	}
	
	private function construct() {
		$this->setBackground ( "#efefef" );
		$this->setFontFamily ( "Verdana" );
		$this->setpaddingBottom("20px");
		$this->setMargin("20px");
		$this->setBorder("2px","solid");
	}

}