<?php

class ObjectPreprocessor extends Preprocessor {

	public function __construct() {
		parent::__construct ( true, "objects/raw", "objects/processed" );
		$this->addInjection ( new DatabaseObjectInjection () );
	}

	public function getFileMetrics($file) {
		return md5 ( file_get_contents ( $file ) );
	}

}