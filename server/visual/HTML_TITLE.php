<?php
class HTML_TITLE extends HTML_PAIR_ELEMENT {

	private $title;
	
	public function __construct() {
		parent::__construct("title");
		$this->title=new HTML_TEXT("Project Cassandra");
		$this->addElement($this->title);
	}

	public function setTitle($title) {
		$this->title->setText($title);
	}
	
}