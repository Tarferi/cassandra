<?php

class PrivateMessage extends DatabaseObject {
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 * @Getter
	 */
	private $id;
	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $senderID;
	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $receiverID;
	/**
	 * @ColumnType("text")
	 * @Getter
	 */
	private $title;
	/**
	 * @ColumnType("text")
	 * @Getter
	 */
	private $content;
	
	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $creationDate;

	// @inject
}