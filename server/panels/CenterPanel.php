<?php

class CenterPanel extends HTML_CENTER {
	private $title;
	private $contents;

	public function __construct($title = false, $contents = false) {
		parent::__construct ();
		$this->construct ();
		if ($contents !== false) {
			if (is_string ( $contents )) {
				$this->contents->addElement ( new HTML_TEXT ( $contents ) );
			} else {
				$this->contents->addElement ( $contents );
			}
		}
		if ($title !== false) {
			if (is_string ( $title )) {
				$this->title->setTitle ( $title );
			}
		}
	}

	public function setTitle($title) {
		$this->title->setTitle ( $title );
	}

	public function getRepresentationObject() {
		return $this->contents;
	}

	public function addElement($object) {
		$this->contents->addElement ( $object );
	}

	public function addContents($contents) {
		$this->addElement ( $contents );
	}

	private function construct() {
		$this->contents = new BlockPanel ();
		parent::addElement ( $this->contents );
		$this->title = new TitlePanel ( "" );
		$this->title->setBorderBottom ( "1px", "solid" );
		$this->addElement ( $this->title );
		$this->addElement ( new HTML_BR () );
		$this->setWidth ( "80%" );
	}

}