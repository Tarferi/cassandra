<?php
include_once 'common.php';
requireLogin ();
Settings::getTitle ()->setTitle ( "Cassandra Modules" );
if (! isset ( $_GET ["module"] )) {
	redirect ( "dashboard.php" );
}
$moduleClassName = $_GET ["module"];
$modules = ModuleList::getModules ( Session::get ()->getUser () );
$found = false;
foreach ( $modules as $module ) {
	if ($module->getClassName () == $moduleClassName) {
		$found = $module;
	}
}
if (! $found) {
	$body = Settings::getBody ();
	$panel = new TitleBlockPanel ( "Cassandra Modules", new NameCode ( "Module \"" . $moduleClassName . "\" could not be found :(." ) );
	$body->addElement ( new HTML_CENTER ( $panel ) );
	Settings::getBody ()->addElement ( new HTML_CENTER ( new HTML_A ( "Back to dashboard", "dashboard.php", "_self" ) ) );
	die ();
}
$module = $found;
$data = $module->getModuleData ();
if ($data === false) {
	Settings::getBody ()->addElement ( new HTML_CENTER ( new TitleBlockPanel ( "Module loader", new NameCode ( "Failed to load module " . $module->getName () ) ) ) );
}
Settings::getBody ()->addElement ( new HTML_CENTER ( new HTML_A ( "Back to dashboard", "dashboard.php", "_self" ) ) );