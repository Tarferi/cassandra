<?php

class Topic extends DatabaseObject {
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 * @Getter
	 */
	private $id;

	/**
	 * @ColumnType("text")
	 * @Getter
	 * @Setter
	 */
	private $topicName;
	
	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $ownerID;
	
	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $creationDate;
	
	// @inject
}