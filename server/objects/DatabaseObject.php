<?php

class DatabaseTable {
	private $name;
	private $columns = array ();

	public function __construct($name) {
		$this->name = $name;
	}

	public function getTableName() {
		return $this->name;
	}

	public function addColumn($col) {
		$this->columns [] = $col;
	}

	public function getColumns() {
		return $this->columns;
	}

}

class DatabaseColumn {
	private $table;
	private $type;
	private $primary;
	private $allowNull;
	private $name;

	public function getColumnName() {
		return $this->name;
	}

	public function getTable() {
		return $this->table;
	}

	public function getType() {
		return $this->type;
	}

	public function isPrimary() {
		return $this->primary;
	}

	public function isNullAllowed() {
		return $this->allowNull;
	}

	public function __construct($table, $name, $type, $primary = false, $allowNull = false) {
		$this->table = $table;
		$this->type = $type;
		$this->name = $name;
		$this->primary = $primary;
		$this->allowNull = $allowNull;
	}

}

abstract class DatabaseObject {
	private $__tableName;
	private $__className;

	protected static function classNameToTableName($className) {
		return Settings::DatabasePrefix . $className;
	}

	protected static function TableNameToClassName($tableName) {
		return substr ( $tableName, strlen ( Settings::DatabasePrefix ) );
	}

	protected static function searchMatch($className, $row, $single = false) {
		$rc = new ReflectionClass ( $className );
		$replacements = array ();
		$query = array ();
		$query [] = "SELECT";
		$query [] = "*";
		$query [] = "FROM";
		$query [] = "`?`";
		$replacements [] = static::classNameToTableName ( $className );
		if (count ( $row ) > 0) {
			$query [] = "WHERE";
			$selects = array ();
			foreach ( $row as $key => $val ) {
				$selects [] = "`?`='?'";
				$replacements [] = $key;
				$replacements [] = $val;
			}
			$query [] = implode ( " AND ", $selects );
		}
		if ($single) {
			$query [] = "LIMIT 1";
		}
		$query = implode ( " ", $query );
		$db = Settings::getDatasource ()->getSource ();
		$res = $db->selectQuery ( $query, $replacements );
		if (is_array ( $res )) {
			$ret = array ();
			foreach ( $res as $r ) {
				$ar = ( array ) $r;
				$rc = new ReflectionClass ( $className );
				$b = $rc->newInstanceArgs ( array (
						$ar 
				) );
				$ret [] = $b;
			}
			return $single ? count ( $ret ) > 0 ? $ret [0] : false : $ret;
		}
		return false;
	}

	private static function getDerivedTables() {
		$objects = array ();
		foreach ( get_declared_classes () as $class ) {
			if (is_subclass_of ( $class, "DatabaseObject" ))
				$objects [] = $class;
		}
		$tables = array ();
		foreach ( $objects as $obj ) {
			$table = new DatabaseTable ( static::classNameToTableName ( $obj ) );
			$rc = new ReflectionAnnotatedClass ( $obj );
			$props = $rc->getProperties ();
			foreach ( $props as $prop ) {
				$name = $prop->getName ();
				$primary = $prop->getAnnotation ( "Primary" ) !== false;
				$allownull = $prop->getAnnotation ( "AllowNull" ) !== false;
				$type = $prop->getAnnotation ( "ColumnType" )->value;
				$col = new DatabaseColumn ( $table, $name, $type, $primary, $allownull );
				$table->addColumn ( $col );
			}
			$tables [] = $table;
		}
		return $tables;
	}

	public static function removeDatabase() {
		$tables = DatabaseObject::getDerivedTables ();
		$queries = array ();
		foreach ( $tables as $table ) {
			$queries [] = array (
					"DROP TABLE IF EXISTS `?`",
					array (
							$table->getTableName () 
					) 
			);
		}
		$db = Settings::getDatasource ()->getSource ();
		foreach ( $queries as $queryd ) {
			$query = $queryd [0];
			$replacements = $queryd [1];
			$db->executeQuery ( $query, $replacements );
		}
	}

	public static function recreateDatabase() {
		DatabaseObject::removeDatabase ();
		DatabaseObject::createDatabase ();
	}

	public static function createDatabase() {
		$tables = DatabaseObject::getDerivedTables ();
		$queries = array ();
		foreach ( $tables as $table ) {
			$replacements = array (
					$table->getTableName () 
			);
			$columns = array ();
			$cols = $table->getColumns ();
			$primary = false;
			foreach ( $cols as $col ) {
				$columns [] = "`?` " . $col->getType () . ($col->isNullAllowed () ? "" : " NOT NULL") . ($col->isPrimary () ? " AUTO_INCREMENT" : "");
				$replacements [] = $col->getColumnName ();
				if ($col->isPrimary ()) {
					$primary = $col;
				}
			}
			if ($primary === false) {
				throw new DatabaseException ();
			}
			$columns [] = "PRIMARY KEY (`?`)";
			$replacements [] = $primary->getColumnName ();
			$columns = implode ( ",\n", $columns );
			$query = array (
					"CREATE TABLE IF NOT EXISTS `?` (\n" . $columns . "\n) ",
					$replacements 
			);
			$queries [] = $query;
		}
		$db = Settings::getDatasource ()->getSource ();
		foreach ( $queries as $queryd ) {
			$query = $queryd [0];
			$replacements = $queryd [1];
			$db->executeQuery ( $query, $replacements );
		}
	}
	/*
	 * protected static function fromID($id, $obj) { $res = Settings::getDatasource ()->getRowsByTableAndID ( strtolower ( $obj ), $id ); if (count ( $res ) == 1) { return new $onj ( $res [0] ); } return false; }
	 */
	public function save() {
		Settings::getDatasource ()->saveObject ( $this );
	}

	public function create() {
		Settings::getDatasource ()->createObject ( $this );
	}

	public function __construct($rows) {
		$this->__tableName = static::classNameToTableName ( get_class ( $this ) );
		$this->__className = get_class ( $this );
		$reflectionClass = new ReflectionClass ( $this->__className );
		foreach ( $rows as $key => $value ) {
			$vl = $reflectionClass->getProperty ( $key );
			$orin = $vl->isPublic ();
			$vl->setAccessible ( true );
			$vl->setValue ( $this, $value );
			$vl->setAccessible ( $orin );
		}
	}

	public function getCreateSQL() {
		$fields = array ();
		$values = array ();
		$replacements = array (
				$this->__tableName 
		);
		$rc = new ReflectionClass ( $this->__className );
		$props = $rc->getProperties ();
		$vls = array ();
		foreach ( $props as $prop ) {
			$orin = $prop->isPublic ();
			$prop->setAccessible ( true );
			$name = $prop->getName ();
			$val = $prop->getValue ( $this );
			$prop->setAccessible ( $orin );
			$vls [$name] = $val;
		}
		$id = false;
		foreach ( $vls as $val => $value ) {
			if (substr ( $val, 0, 2 ) != "__" && $val != "id") {
				$fields [] = $val;
				$values [] = "'?'";
				$replacements [] = $value;
			} else if ($val == "id") {
				$id = $val;
			}
		}
		$fields = implode ( ", ", $fields );
		$values = implode ( ", ", $values );
		if ($id === false) {
			throw new DatabaseException ();
		}
		$sql = "INSERT INTO `?` (" . $fields . ") VALUES (" . $values . ")";
		return array (
				$sql,
				$replacements 
		);
	}

	public function getSaveSQL() {
		$updates = array ();
		$replacements = array (
				$this->__tableName 
		);
		$rc = new ReflectionClass ( $this->__className );
		$props = $rc->getProperties ();
		foreach ( $props as $prop ) {
			$orin = $prop->isPublic ();
			$prop->setAccessible ( true );
			$name = $prop->getName ();
			$val = $prop->getValue ( $this );
			$prop->setAccessible ( $orin );
			$vls [$name] = $val;
		}
		$id = false;
		foreach ( $vls as $val => $value ) {
			if (substr ( $val, 0, 2 ) != "__" && $val != "id") {
				$updates [] = $val . "='?'";
				$replacements [] = $value;
			} else if ($val == "id") {
				$id = $val;
			}
		}
		if ($id === false) {
			throw new DatabaseException ();
		}
		$fields = implode ( ", ", $updates );
		$sql = "UPDATE `?` SET " . $fields . " WHERE id='?'";
		$replacements [] = $id;
		return array (
				$sql,
				$replacements 
		);
	}

}