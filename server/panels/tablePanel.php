<?php

class tablePanel extends HTML_DIV {
	private $rows = 0;
	private $cols = 0;
	private $ar = array ();
	private $added = false;
	private $maxcol = 0;
	private $maxrow = 0;
	private $futureCells = array ();
	private $futureRows = array ();
	private $futureTable;

	public function __construct() {
		parent::__construct ();
		$this->construct ();
	}

	private function construct() {
		$this->futureTable = new HTML_TABLE ();
	}

	public function toString() {
		if (! $this->added) {
			$this->add ();
			$this->added = true;
		}
		return parent::toString ();
	}

	private function add() {
		$table = $this->futureTable;
		$ar = $this->ar;
		$aq = array ();
		for($row = 0; $row <= $this->maxrow; $row ++) {
			for($col = 0; $col <= $this->maxcol; $col ++) {
				$aq [$row] [$col] = new HTML_TD ();
			}
		}
		foreach ( $this->futureCells as $row => $cell ) {
			foreach ( $cell as $col => $data ) {
				$aq [$row] [$col] = $data;
			}
		}
		
		$maxcoldata = array ();
		$maxrowdata = 0;
		for($row = 0; $row <= $this->maxrow; $row ++) {
			$maxcoldata [$row] = 0;
			if (isset ( $ar [$row] )) {
				$maxrowdata = $row;
				for($col = 0; $col <= $this->maxcol; $col ++) {
					if (isset ( $ar [$row] [$col] )) {
						$data = $ar [$row] [$col];
						$object = $data [0];
						$rowspan = $data [1];
						$colspan = $data [2];
						$cl = $aq [$row] [$col];
						$data = $aq [$row] [$col];
						/*
						 * for($rs = 0; $rs < $rowspan; $rs ++) { for($cs = 0; $cs < $colspan; $cs ++) { //$aq [$row + $rs] [$col + $cs] = false; } }
						 */
						$aq [$row] [$col] = $cl;
						if ($rowspan > 1) {
							$data->addAttribute ( "rowspan", $rowspan );
						}
						if ($colspan > 1) {
							$data->addAttribute ( "colspan", $colspan );
						}
						$maxcoldata [$row] = $col;
						$cl->addElement ( $object );
					}
				}
			}
		}
		for($row = 0; $row <= $maxrowdata; $row ++) {
			if (isset ( $this->futureRows [$row] )) {
				$rw = $this->futureRows [$row];
			} else {
				$rw = new HTML_TR ();
			}
			for($col = 0; $col <= $maxcoldata [$row]; $col ++) {
				$cl = $aq [$row] [$col];
				$rw->addElement ( $cl );
			}
			$table->addElement ( $rw );
		}
		$this->addElement ( $table );
	}

	public function setCollapseBorder($collapse) {
		$this->futureTable->addRawStyle ( "border-collapse", $collapse ? "collapse" : "" );
	}

	public function getTable() {
		return $this->futureTable;
	}

	public function getColumnAt($row, $col) {
		if (! isset ( $this->futureCells [$row] )) {
			$this->futureCells [$row] = array ();
		}
		if (! isset ( $this->futureCells [$row] [$col] )) {
			$this->futureCells [$row] [$col] = new HTML_TD ();
		}
		return $this->futureCells [$row] [$col];
	}

	public function getRowAt($row) {
		if (! isset ( $this->futureRows [$row] )) {
			$this->futureRows [$row] = new HTML_TR ();
		}
		return $this->futureRows [$row];
	}

	public function addComponent($object, $row, $col, $rowspan = 1, $colspan = 1, $width = false, $height = false) {
		$rw = $row + $rowspan - 1;
		$cl = $col + $colspan - 1;
		if ($rw > $this->maxrow) {
			$this->maxrow = $rw;
		}
		if ($cl > $this->maxcol) {
			$this->maxcol = $cl;
		}
		if (! isset ( $this->ar [$row] )) {
			$this->ar [$row] = array ();
		}
		if (! isset ( $this->ar [$row] [$col] )) {
			$this->ar [$row] [$col] = array (
					$object,
					$rowspan,
					$colspan 
			);
		}
		if ($width !== false) {
			$object->setWidth ( $width );
		}
		if ($height !== false) {
			$object->setHeight ( $height );
		}
	}

}