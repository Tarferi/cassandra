<?php
$dir = scandir ( "visual" );

include "visual/HTML_ELEMENT.php";
foreach ( $dir as $file ) {
	if ($file == "." || $file == "..") {
		continue;
	}
	include_once "visual/" . $file;
}

include "panels/horizontalPanel.php";
include "panels/tablePanel.php";
include "panels/TitlePanel.php";
include "panels/CenterPanel.php";
include "panels/BlockPanel.php";
include "panels/TitleBlockPanel.php";

$dir = scandir ( "panels/components" );
foreach ( $dir as $file ) {
	if ($file == "." || $file == ".." || is_dir ( $file )) {
		continue;
	}
	include_once "panels/components/" . $file;
}

$dir = scandir ( "panels/functionPanels" );
foreach ( $dir as $file ) {
	if ($file == "." || $file == ".." || is_dir ( $file )) {
		continue;
	}
	include_once "panels/functionPanels/" . $file;
}