<?php
class NameCode extends HTML_SPAN {
	private $txt;

	public function __construct($text) {
		parent::__construct ();
		$txt = new HTML_TEXT ( $text );
		$this->setFontFamily ( "Courier" );
		$this->setFontSize ( "14pt" );
		$this->addElement($txt);
	}

}