<?php

abstract class Database {
	private $link = false;

	public function selectQuery($query, $replacements) {
		if ($this->link === false) {
			$this->link = $this->handleInitDatabase ();
			if ($this->link === false || $this->link === null) {
				throw new DatabaseException ();
			}
		}
		$query = $this->handleReplacements ( $this->link, $query, $replacements );
		if ($query != false) {
			return $this->commitSelectQuery ( $this->link, $query );
		}
		return false;
	}

	protected abstract function handleInitDatabase();

	protected abstract function handleReplacements($link, $query, $replacements);

	protected abstract function commitExecuteQuery($link, $query);

	protected abstract function commitSelectQuery($link, $query);

	public function executeQuery($query, $replacements) {
		if ($this->link === false) {
			$this->link = $this->handleInitDatabase ();
			if ($this->link === false || $this->link === null) {
				throw new DatabaseException ();
			}
		}
		$query = $this->handleReplacements ( $this->link, $query, $replacements );
		if ($query != false) {
			return $this->commitExecuteQuery ( $this->link, $query );
		}
		return false;
	}

}

class DatabaseException extends Exception {

}