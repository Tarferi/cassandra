<?php

class HiddenValue extends HTML_INPUT {

	public function __construct($name, $value) {
		parent::__construct ( "hidden" );
		$this->setName ( $name );
		$this->setValue ( $value );
	}

}