<?php

class HTML_FORM extends HTML_PAIR_ELEMENT {
	private $listeners = array ();
	private $submittedData = array ();
	private $method;
	private $callable = false;

	private function handleData() {
		global $_GET;
		global $_POST;
		$this->submittedData = $this->method == "POST" ? $_POST : $_GET;
		if (isset ( $this->submittedData [$this->getName ()] )) {
			$this->callable = true;
		}
	}

	private function dispatchListener($listener, $param = false) {
		if ($this->callable) {
			$listener ( $this, $param );
		}
	}

	public function getSubmitedData() {
		return $this->submittedData;
	}

	public function getValueByName($name, $default = false, $checkFunction = false, $checkFunctionIsArray = false) {
		if (isset ( $this->submittedData [$name] )) {
			$value = $this->submittedData [$name];
			if ($checkFunction === false) {
				return $value;
			} else if ($checkFunctionIsArray === false) {
				return $checkFunction ( $value );
			} else {
				return $checkFunction [$name] ( $value );
			}
		} else {
			return $default;
		}
	}

	public function __construct($action, $name = false, $method = "POST") {
		parent::__construct ( "form" );
		$this->method = $method;
		if ($name !== false) {
			$this->addAttribute ( "name", $name );
		}
		$this->addAttribute ( "method", $method );
		$this->addAttribute ( "action", $action );
		$this->addElement ( new HiddenValue ( $name, "submit" ) );
		$this->handleData ();
	}

	public function addListener($listener, $param = false) {
		$this->dispatchListener ( $listener, $param );
	}

}