<?php
include_once "common.php";

$body = Settings::getBody ();
$panel = new CenterPanel ( "Cassandra Registration" );
$panel->addText ( "Registration requires several informations about you", true );
$panel->addText ( "Please read and use correct information during the process", true );
$panel->addSeparator ( "75%" );
$form = new HTML_FORM ( "#", "RegisterForm" );
$tablePanel = new tablePanel ();
$form->addElement ( $tablePanel );
$username = "";
$password = "";
$name = "";
$date = "";
$email = "";

$form->addListener ( function ($formm) {
	global $tablePanel;
	$checks = array (
			"Username" => function ($in) {
				return User::usernameOK ( $in );
			},
			"Password" => function ($in) {
				return User::passwordOK ( $in );
			},
			"Name" => function ($in) {
				return User::nameOK ( $in );
			},
			"Date" => function ($in) {
				return User::dateOK ( $in );
			},
			"Email" => function ($in) {
				return User::emailOK ( $in );
			} 
	);
	$fail = false;
	if (($username = $formm->getValueByName ( "Username", false, $checks, true )) === false) {
		$fail = true;
		$tablePanel->addComponent ( new ErrorText ( "<< Invalid or missing field" ), 0, 2 );
	}
	if (($password = $formm->getValueByName ( "Password", false, $checks, true )) === false) {
		$fail = true;
		$tablePanel->addComponent ( new ErrorText ( "<< Invalid or missing field" ), 1, 2 );
	}
	if (($name = $formm->getValueByName ( "Name", false, $checks, true )) === false) {
		$fail = true;
		$tablePanel->addComponent ( new ErrorText ( "<< Invalid or missing field" ), 2, 2 );
	}
	if (($date = $formm->getValueByName ( "Date", false, $checks, true )) === false) {
		$fail = true;
		$tablePanel->addComponent ( new ErrorText ( "<< Invalid or missing field" ), 3, 2 );
	}
	if (($email = $formm->getValueByName ( "Email", false, $checks, true )) === false) {
		$fail = true;
		$tablePanel->addComponent ( new ErrorText ( "<< Invalid or missing field" ), 4, 2 );
	}
	if (! $fail) {
		$row = array (
				"username" => $username,
				"password" => $password,
				"name" => $name,
				"email" => $email,
				"borndate" => $date 
		);
		$user = new User ( $row );
		$user->create ();
		redirect ( "login.php" );
	}
} );

$tablePanel->addComponent ( new HTML_TEXT ( "Username:" ), 0, 0 );
$tablePanel->addComponent ( new HTML_TEXT ( "Password:" ), 1, 0 );
$tablePanel->addComponent ( new HTML_TEXT ( "Real name:" ), 2, 0 );
$tablePanel->addComponent ( new HTML_TEXT ( "Birthdate:" ), 3, 0 );
$tablePanel->addComponent ( new HTML_TEXT ( "Email:" ), 4, 0 );

$tablePanel->addComponent ( new InputText ( "Username", false, true, "20", $username ), 0, 1, 1, 1, "100%" );
$tablePanel->addComponent ( new InputPassword ( "Password", false, true, "20" ), 1, 1, 1, 1, "100%" );
$tablePanel->addComponent ( new InputText ( "Name", false, true, "20", $name ), 2, 1, 1, 1, "100%" );
$tablePanel->addComponent ( new InputDate ( "Date", false, true, $date ), 3, 1, 1, 1, "100%" );
$tablePanel->addComponent ( new InputText ( "Email", false, true, "20", $email ), 4, 1, 1, 1, "100%" );

$button = new Button1 ();
$button->setName ( "Register" );
$button->setValue ( "Register" );
$button->setWidth ( "300px" );
$button->setHeight ( "40px" );

$tablePanel->addComponent ( $button, 5, 0, 1, 2 );

$tablePanel->getColumnAt ( 0, 0 )->setAlign ( "right" );
$tablePanel->getColumnAt ( 1, 0 )->setAlign ( "right" );
$tablePanel->getColumnAt ( 2, 0 )->setAlign ( "right" );
$tablePanel->getColumnAt ( 3, 0 )->setAlign ( "right" );
$tablePanel->getColumnAt ( 4, 0 )->setAlign ( "right" );

$panel->addContents ( $form );

$body->addElement ( $panel );
die ();