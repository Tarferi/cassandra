<?php

class User extends DatabaseObject {
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 * @Getter
	 */
	private $id;
	/**
	 * @ColumnType("text")
	 * @Getter
	 * @Setter
	 */
	private $username;
	/**
	 * @ColumnType("text")
	 * @Getter
	 * @Setter
	 */
	private $password;
	/**
	 * @ColumnType("text")
	 * @Getter
	 * @Setter
	 */
	private $name;
	/**
	 * @ColumnType("text")
	 * @Getter
	 * @Setter
	 */
	private $email;
	/**
	 * @ColumnType("int")
	 * @Getter
	 * @Setter
	 */
	private $borndate;

	public static function getByCredentials($username, $password) {
		return static::searchMatch ( array (
				"username" => $username,
				"password" => $password 
		), true );
	}

	private static function match_fully($string, $regex) {
		$q = preg_match ( $regex, $string );
		return $q === 1;
		if (count ( $q ) == 0) {
			if ($q [0] == $string) {
				return true;
			}
		}
		return false;
	}

	public static function usernameOK($username) {
		if (is_string ( $username )) {
			if (strlen ( $username ) > 0 && strlen ( $username ) < 20) {
				if (User::match_fully ( $username, "/\w+/" )) {
					return $username;
				}
			}
		}
		return false;
	}

	public static function passwordOK($username) {
		if (is_string ( $username )) {
			if (strlen ( $username ) > 0 && strlen ( $username ) < 20) {
				if (User::match_fully ( $username, "/\w+/" )) {
					return $username;
				}
			}
		}
		return false;
	}

	public static function nameOK($username) {
		if (is_string ( $username )) {
			if (strlen ( $username ) > 0 && strlen ( $username ) < 20) {
				if (User::match_fully ( $username, "/\w+ \w+/" )) {
					return $username;
				}
			}
		}
		return false;
	}

	public static function dateOK($username) {
		return $username;
	}
	
	public function hasPermission($permission) {
		return true;
	}

	public static function emailOK($username) {
		if (is_string ( $username )) {
			if (strlen ( $username ) > 0 && strlen ( $username ) < 20) {
				if (User::match_fully ( $username, "/\w+\@\w+\.\w+/" )) {
					return $username;
				}
			}
		}
		return false;
	}
	
	// @inject
}