<?php

class LoginPanel extends HTML_SPAN {
	public $qthis;

	public function __construct() {
		parent::__construct ();
		$this->construct ();
	}

	public function registerPressed($form) {
		redirect ( "register.php" );
		die ();
	}

	public function loginPressed($form) {
		$username = $form->getValueByName ( "username" );
		$password = $form->getValueByName ( "password" );
		$user = User::getByCredentials ( $username, $password );
		if ($user) {
			$sess = Session::getByUser ( $user );
			$sess->save ();
			$this->logged = true;
			return true;
		}
	}
	private $logged = false;

	public function isLogged() {
		return $this->logged;
	}

	private function construct() {
		$form = new HTML_FORM ( "#", "loginFrm" );
		$qthis = new tablePanel ();
		$this->qthis = $qthis;
		$this->addElement ( $form );
		$form->addElement ( $qthis );
		$form->setHeight ( "39px" );
		$qthis->setBorder ( "1px", "solid" );
		$button = new Button1 ( "Btn1" );
		$button->setName ( "Login" );
		$button->setValue ( "Login" );
		$button->setWidth ( "75px" );
		$button->setHeight ( "100%" );
		$button2 = new Button1 ( "Btn2" );
		$button2->setName ( "Register" );
		$button2->setValue ( "Register" );
		$button2->setWidth ( "75px" );
		$button2->setHeight ( "100%" );
		$qthis->addComponent ( new HTML_TEXT ( "Username:" ), 0, 0 );
		$qthis->addComponent ( new HTML_TEXT ( "Password:" ), 1, 0 );
		$qthis->addComponent ( new HTML_INPUT ( "text", "username" ), 0, 1 );
		$qthis->addComponent ( new HTML_INPUT ( "password", "password" ), 1, 1 );
		$qthis->addComponent ( $button, 0, 2, 2, 1 );
		$form->addListener ( function ($form, $qthis) {
			if ($form->getValueByName ( "Register", False ) !== False) {
				$qthis->registerPressed ( $form );
			} else if ($form->getValueByName ( "Login", False ) !== False) {
				if (($Username = $form->getValueByName ( "username" )) !== False) {
					if (($Password = $form->getValueByName ( "password" )) !== False) {
						if ($qthis->loginPressed ( $form )) {
							return;
						}
					}
				}
				$qthis->qthis->addComponent ( new HTML_TEXT ( "Invalid username or password" ), 2, 0, 1, 4 );
				$qthis->qthis->getColumnAt ( 2, 0 )->setBorderTop ( "1px", "solid" );
				$qthis->qthis->getColumnAt ( 2, 0 )->setAlign ( "center" );
			}
		}, $this );
		$qthis->addComponent ( $button2, 0, 3, 2, 1 );
	}

}