<?php

class Session {
	private $lastAction;
	private $userID;
	private $cookie;
	private static $currentSession = false;

	const SESSION_DIR = "sessions";

	public static function get() {
		if (static::$currentSession === false) {
			die ( "Accessing empty session" );
		} else {
			return static::$currentSession;
		}
	}

	public function getUser() {
		static $user = false;
		if ($user === false) {
			$user = User::getByID ( $this->userID, true );
		}
		return $user;
	}

	private static function cookieToFileName($cookie) {
		return Session::SESSION_DIR . "/" . md5 ( "Cassie <3 " . $cookie ) . ".jsn";
	}

	private function __construct($userID, $cookie, $lastAction = false, $isNotCurrent = false) {
		if ($isNotCurrent === false) {
			static::$currentSession = $this;
		}
		Cookie::addCookie ( Settings::COOKIE_NAME, $cookie );
		$this->cookie = $cookie;
		$this->userID = $userID;
		$this->lastAction = $lastAction === false ? Settings::getTime () : $lastAction;
	}

	public static function getByUser($user) {
		$cookie = "Cassie_" . md5 ( rand ( 0, 1000 ) . rand ( 500, 50000 ) . rand ( 500, 10000 ) );
		$sess = new Session ( $user->getID (), $cookie );
		return $sess;
	}

	public function save() {
		if (! file_exists ( Session::SESSION_DIR )) {
			mkdir ( Session::SESSION_DIR );
		}
		$file = static::cookieToFileName ( $this->cookie );
		file_put_contents ( $file, json_encode ( array (
				"lastAction" => $this->lastAction,
				"userID" => $this->userID 
		) ) );
	}

	private function userExists() {
		return User::getByID ( $this->userID, true ) !== False;
	}

	public static function isLogged() {
		if (static::$currentSession !== false) {
			return static::$currentSession->userExists ();
		}
		return false;
	}

	public static function fromCookie($cookie) {
		$file = Session::cookieToFileName ( $cookie );
		if (file_exists ( $file )) {
			$q = file_get_contents ( $file );
			if ($q) {
				$b = json_decode ( $q, true );
				if ($b) {
					$gd = function ($array, $key) {
						if (isset ( $array [$key] )) {
							return $array [$key];
						} else {
							return false;
						}
					};
					if ($lastDat = $gd ( $b, "lastAction" )) {
						if ($userID = $gd ( $b, "userID" )) {
							if ($lastDat + Settings::SESSION_EXPIRE < Settings::getTime ()) {
								unlink ( $file );
								return false;
							} else {
								$session = new Session ( $userID, $cookie, $lastDat, false );
								if ($session->userExists ()) {
									return $session;
								} else {
									unlink ( $file );
									return false;
								}
							}
						}
					}
				}
			}
		}
		return false;
	}

}