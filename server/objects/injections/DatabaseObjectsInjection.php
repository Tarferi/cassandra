<?php

class DatabaseObjectInjection extends Injection {

	public function __construct() {
		parent::__construct ( true, true );
	}

	/**
	 * @Inject
	 */
	public function getClassName() {
		return get_class ( $this );
	}

	/**
	 * @Inject
	 */
	public static function searchMatch($row, $single = false) {
		return parent::searchMatch ( get_called_class (), $row, $single );
	}

	/**
	 * @Inject
	 */
	public static function getByID($id, $single = true) {
		return static::searchMatch ( array (
				"id" => $id 
		), $single );
	}

}