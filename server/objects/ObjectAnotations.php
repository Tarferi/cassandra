<?php

/**
 * @Target("property")
 */
class ColumnType extends Annotation {
	public $value;

}

/**
 * @Target("property")
 */
class Primary extends Annotation {

}

/**
 * @Target("property")
 */
class AllowNull extends Annotation {

}

/**
 * @Target("method")
 */
class Inject extends Annotation {

}