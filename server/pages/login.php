<?php
include_once "common.php";
if (! Session::isLogged ()) {
	$body = Settings::getBody ();
	$topPanel = new CenterPanel ( "Cassandra" );
	$loginPanel = new LoginPanel ();
	$tablePanel = new tablePanel ();
	$tablePanel->addComponent ( new HTML_TEXT ( "You are not logged in :/" ), 0, 0 );
	$tablePanel->addComponent ( $loginPanel, 0, 1 );
	$topPanel->addContents ( $tablePanel );
	$tablePanel->getColumnAt ( 0, 0 )->setBorderBottom ( "1px", "solid" );
	$tablePanel->getColumnAt ( 0, 1 )->setValign ( "bottom" );
	$body->addElement ( $topPanel );
	if ($loginPanel->isLogged ()) {
		Settings::clearHtmlComplete();
	} else {
		die ();
	}
}