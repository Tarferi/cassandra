<?php
class ModuleInjection extends Injection {
	
	public function __construct() {
		parent::__construct ( true, true );
	}
	
	/**
	 * @Inject
	 */
	public function getClassName() {
		return get_class ( $this );
	}
	
	/**
	 * @Inject
	 */
	public static function getPermissionName() {
		return "module.".get_called_class();
	}
}