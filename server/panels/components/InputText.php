<?php

class InputText extends HTML_INPUT {

	public function __construct($name = false, $id = false, $required = false, $size = false, $value = false) {
		parent::__construct ( "text" );
		$this->construct ();
		if ($name !== false) {
			$this->setName ( $name );
		}
		if ($id !== false) {
			$this->setID ( $id );
		}
		if ($required !== false) {
			$this->addAttribute ( "required", $required );
		}
		if ($size !== false) {
			$this->addAttribute ( "maxlength", $size );
		}
		if ($value !== false) {
			$this->addAttribute ( "value", $value );
		}
	}

	private function construct() {
		$this->setBackground ( "#eaeaea" );
		$this->setOutline ( "none" );
		$this->setBorder ( "1px", "solid" );
		$this->setpadding ( "3px" );
	}

}