<?php
fixCWD ();
include_once "includes.php";

Preprocessor::handlePreprocessing ();

ob_start ( 'fatal_error_handler' );

function fixCWD() {
	$cwd = str_replace ( "\\", "/", getcwd () );
	$len = strlen ( $cwd );
	$endstr = "/pages";
	$lenstr = strlen ( $endstr );
	if (substr ( $cwd, $len - $lenstr, $lenstr ) == $endstr) {
		$ncwd = substr ( $cwd, 0, $len - $lenstr );
		chdir ( $ncwd );
	}
}

function CassieFinalizeObjects() {
	Settings::restoreCWD ();
	static $finalized = false;
	if (! $finalized) {
		$finalized = true;
		$html = Settings::getHtml ();
		$body = Settings::getBody ();
		$head = Settings::getHead ();
		$title = Settings::getTitle ();
		$html->addElement ( $head );
		$html->addElement ( $body );
		$head->addElement ( $title );
	}
}

function CassieHandleEmptyPage() {
	Settings::restoreCWD ();
	$html = Settings::getHtml ();
	$body = Settings::getBody ();
	$head = Settings::getHead ();
	if ($body->isEmpty ()) {
		if ($head->isEmpty ()) {
			$panel = new CenterPanel ();
			$panel->setTitle ( "Cassandra" );
			$panel->addText ( "You got us unprepared!", false, true );
			$panel->addText ( "This page either does not exist, or is unavailable for you!", true, true );
			$panel->addElement ( new HTML_BR () );
			$panel->addElement ( new HTML_HR () );
			$panel->addText ( "Please contant any of the moderators for more informations", true, true );
			$body->addElement ( $panel );
		}
	}
}

function fatal_error_handler($buffer) {
	Cookie::commit ();
	Settings::restoreCWD ();
	$error = error_get_last ();
	if ($error ['type'] > 0) {
		return $buffer;
		Settings::clearHtmlComplete ();
		CassieFinalizeObjects ();
		$body = Settings::getBody ();
		$panel = new CenterPanel ();
		$panel->setTitle ( "Cassandra" );
		$panel->addText ( "Cassie failed to operate carefully :'(", false, true );
		$panel->addText ( "This error has been reported and will be sorted out as soon as possible", true, true );
		$panel->addElement ( new HTML_HR () );
		$panel->addText ( "Details:", true, true );
		$panel->addText ( "File:", true, false );
		$panel->addText ( $error ["file"], false, false );
		$panel->addText ( "(Line " . $error ["line"] . ")", false, true );
		$panel->addText ( "Message:", true, false );
		$panel->addText ( $error ["message"], false, true );
		$body->addElement ( $panel );
		return Settings::getHtml ()->toString ();
	}
	return $buffer;
}

function CassieExit() {
	try {
		Settings::restoreCWD ();
		CassieHandleEmptyPage ();
		CassieFinalizeObjects ();
		echo Settings::getHtml ()->toString ();
	} catch ( Exception $e ) {
		echo "Fail to fetch final Cassie data :'(";
	}
}

function CassieError($errno, $errstr, $errfile, $errline) {
	Settings::restoreCWD ();
	echo "Cassie failed to operate carefuly :'(";
}

register_shutdown_function ( "CassieExit" );

$session = false;

if (($sessionID = Cookie::getOrDefault ( Settings::COOKIE_NAME, false )) !== false) {
	Session::fromCookie ( $sessionID );
}

function redirect($page, $includePage = false) {
	if ($includePage) {
		header ( "Location: pages/" . $page );
		die ( "<script>document.location.href=\"pages/" . $page . "\"</script>" );
	} else {
		header ( "Location: " . $page );
		die ( "<script>document.location.href=\"" . $page . "\"</script>" );
	}
}

function requireLogin() {
	include_once "pages/login.php";
}
