<?php

class Cookie {
	private static $cookies = array ();

	public static function getOrDefault($name, $default) {
		if (isset ( static::$cookies [$name] )) {
			return static::$cookies [$name];
		} else {
			return $default;
		}
	}

	public static function addCookie($name, $value) {
		static::$cookies [$name] = $value;
	}

	public static function removeCookie($name) {
		static::$cookies [$name] = false;
	}

	public static function commit() {
		var_dump ( static::$cookies );
		foreach ( static::$cookies as $name => $value ) {
			if ($value === false) {
				setcookie ( $name, null, - 1, "/" );
			} else {
				setcookie ( $name, $value, - 1, "/" );
			}
		}
	}

	public static function load($data) {
		static::$cookies = $data;
	}

}

Cookie::load ( $_COOKIE );