<?php

class HTML_ELEMENT {
	private $styles = array ();
	private $handlers = array ();
	private $rawstyles = array ();
	private $name;
	private $attributes = array ();
	private $indent = 0;

	public function addAttribute($name, $value) {
		if (strlen ( $name ) > 0) {
			$name = ucfirst ( $name );
			$this->getRepresentationObject ()->attributes [strtolower ( $name )] = array (
					$name,
					htmlspecialchars ( $value ) 
			);
		}
	}

	public function addClickServerHandler($handler, $params = null) {
		$token = md5 ( $this->getName () );
		$this->addAttribute ( "onClick", "document.write(\"<form id=rfrmQ method=POST action=#><input type=hidden name=" . "frm_" . $token . " value=" . "frm_" . $token . " /></form>\");document.getElementById('rfrmQ').submit();" );
		global $_POST;
		if (isset ( $_POST ["frm_" . $token] )) {
			$handler ( $params );
		}
	}

	public function getName($default = false) {
		// echo "Getting name of ".$this->name."\n<br />";
		return $this->getAttribute ( "name", $default );
	}

	protected function getAttribute($name, $default = false) {
		if (isset ( $this->getRepresentationObject ()->attributes [strtolower ( $name )] )) {
			return htmlspecialchars_decode ( $this->getRepresentationObject ()->attributes [strtolower ( $name )] [1] );
		} else {
			return $default;
		}
	}

	public function setOutline($outline) {
		$this->getRepresentationObject ()->addRawStyle ( "outline", $outline );
	}

	public function setOutlineWidth($outline) {
		$this->getRepresentationObject ()->addRawStyle ( "outline-width", $outline );
	}

	public function setOutlineStyle($outline) {
		$this->getRepresentationObject ()->addRawStyle ( "outline-style", $outline );
	}

	public function setOutlineColor($outline) {
		$this->getRepresentationObject ()->addRawStyle ( "outline-color", $outline );
	}

	public function setName($name) {
		// /echo "Setting name of ".$this->name." to ".$name."\n<br />";
		$this->getRepresentationObject ()->addAttribute ( "name", $name );
	}

	public function setID($id) {
		$this->getRepresentationObject ()->addAttribute ( "id", $id );
	}

	public function getRepresentationObject() {
		return $this;
	}

	public function setWidth($Width) {
		$this->getRepresentationObject ()->addRawStyle ( "width", $Width );
	}

	public function setHeight($Height) {
		$this->getRepresentationObject ()->addRawStyle ( "height", $Height );
	}

	public function setBackground($Color) {
		$this->getRepresentationObject ()->addRawStyle ( "background-color", $Color );
	}

	public function setForeground($color) {
		$this->getRepresentationObject ()->addRawStyle ( "color", $color );
	}

	public function setFontFamily($fontFamily) {
		$this->getRepresentationObject ()->addRawStyle ( "font-family", $fontFamily );
	}

	public function setDisplay($display) {
		$this->getRepresentationObject ()->addRawStyle ( "display", $display );
	}

	public function setFontSize($fontSize) {
		$this->getRepresentationObject ()->addRawStyle ( "font-size", $fontSize );
	}

	public function setMargin($margin) {
		$this->getRepresentationObject ()->addRawStyle ( "margin", $margin );
	}

	public function setMarginLeft($margin) {
		$this->getRepresentationObject ()->addRawStyle ( "margin-left", $margin );
	}

	public function setMarginRight($margin) {
		$this->getRepresentationObject ()->addRawStyle ( "margin-right", $margin );
	}

	public function setMarginTop($margin) {
		$this->getRepresentationObject ()->addRawStyle ( "margin-top", $margin );
	}

	public function setMarginBottom($margin) {
		$this->getRepresentationObject ()->addRawStyle ( "margin-bottom", $margin );
	}

	public function setpadding($padding) {
		$this->getRepresentationObject ()->addRawStyle ( "padding", $padding );
	}

	public function setPaddingLeft($padding) {
		$this->getRepresentationObject ()->addRawStyle ( "padding-left", $padding );
	}

	public function setPaddingRight($padding) {
		$this->getRepresentationObject ()->addRawStyle ( "padding-right", $padding );
	}

	public function setPaddingTop($padding) {
		$this->getRepresentationObject ()->addRawStyle ( "padding-top", $padding );
	}

	public function setPaddingBottom($padding) {
		$this->getRepresentationObject ()->addRawStyle ( "padding-bottom", $padding );
	}

	public function setBorder($borderWidth = "1px", $borderStyle = "inherit", $borderColor = "#000000") {
		$this->getRepresentationObject ()->addRawStyle ( "border", $borderWidth . " " . $borderStyle . " " . $borderColor );
	}

	public function setBorderLeft($borderWidth = "1px", $borderStyle = "inherit", $borderColor = "#000000") {
		$this->getRepresentationObject ()->addRawStyle ( "border-left", $borderWidth . " " . $borderStyle . " " . $borderColor );
	}

	public function setBorderRight($borderWidth = "1px", $borderStyle = "inherit", $borderColor = "#000000") {
		$this->getRepresentationObject ()->addRawStyle ( "border-right", $borderWidth . " " . $borderStyle . " " . $borderColor );
	}

	public function setBorderTop($borderWidth = "1px", $borderStyle = "inherit", $borderColor = "#000000") {
		$this->getRepresentationObject ()->addRawStyle ( "border-top", $borderWidth . " " . $borderStyle . " " . $borderColor );
	}

	public function setBorderBottom($borderWidth = "1px", $borderStyle = "inherit", $borderColor = "#000000") {
		$this->getRepresentationObject ()->addRawStyle ( "border-bottom", $borderWidth . " " . $borderStyle . " " . $borderColor );
	}

	public function setAlign($align) {
		$this->getRepresentationObject ()->addRawStyle ( "text-align", $align );
	}

	public function setVerticalAlign($valign) {
		$this->getRepresentationObject ()->addRawStyle ( "vertical-align", $valign );
	}

	public function addText($text, $newlineBefore = false, $newlineAfter = false) {
		if ($newlineBefore) {
			$this->addElement ( new HTML_BR () );
		}
		$this->addElement ( new HTML_TEXT ( $text ) );
		if ($newlineAfter) {
			$this->addElement ( new HTML_BR () );
		}
	}

	public function addStyle($style) {
		$this->getRepresentationObject ()->styles [] = $style;
	}

	public function addRawStyle($name, $value) {
		$this->getRepresentationObject ()->rawstyles [$name] = $value;
	}

	protected function __construct($Name) {
		$this->name = $Name;
	}

	protected function getIndentString() {
		$indent = "";
		for($i = 0; $i < $this->indent; $i ++) {
			$indent .= " ";
		}
		return $indent;
	}

	protected function getIndent() {
		return $this->indent;
	}

	protected function getContents() {
		$contents = array ();
		$indent = $this->getIndentString ();
		if (count ( $this->styles ) > 0) {
			$style = implode ( " ", $this->styles );
			$this->addAttribute ( "class", $style );
		}
		if (count ( $this->rawstyles ) > 0) {
			$b = array ();
			foreach ( $this->rawstyles as $name => $value ) {
				$b [] = $name . ":" . $value;
			}
			$rawstyles = implode ( ";", $b );
			$this->addAttribute ( "style", $rawstyles );
		}
		if (count ( $this->handlers ) > 0) {
			foreach ( $this->handlers as $handler ) {
				$this->addAttribute ( $handler [0], $handler [1] );
			}
		}
		foreach ( $this->attributes as $attribute ) {
			$key = $attribute [0];
			$value = $attribute [1];
			$contents [] = $key . "=\"" . $value . "\"";
		}
		return $contents;
	}

	protected function getTagName() {
		return $this->name;
	}

	protected function setIndent($i) {
		$this->indent = $i;
	}

	public function finalize() {
	}
	private $finalized = false;

	public function toString() {
		if (! $this->finalized) {
			$this->finalized = true;
			$this->finalize ();
		}
	}

	public function clear() {
		$this->attributes = array ();
		$this->handlers = array ();
		$this->rawstyles = array ();
		$this->styles = array ();
	}

}

class HTML_PAIR_ELEMENT extends HTML_ELEMENT {
	private $objects = array ();

	public function addSeparator($width = false) {
		$el = new HTML_HR ();
		if ($width !== false) {
			$el->addAttribute ( "width", $width );
		}
		$this->getRepresentationObject ()->addElement ( $el );
	}

	public function getSubElementsCount() {
		return count ( $this->objects );
	}

	public function isEmpty() {
		return $this->getSubElementsCount () == 0;
	}

	public function clear() {
		$this->objects = array ();
		parent::clear ();
	}

	public function toString() {
		parent::toString ();
		$contents = $this->getContents ();
		$indent = $this->getIndentString ();
		$tag1 = $indent . "<" . $this->getTagName ();
		if (count ( $contents ) > 0) {
			$tag1 .= " " . implode ( " ", $contents );
		}
		if (count ( $this->objects ) > 0) {
			$tag1 .= ">";
			$subobjects = array ();
			foreach ( $this->objects as $object ) {
				$object->setIndent ( $this->getIndent () + 1 );
				$subobjects [] = $object->toString ();
			}
			$tag1 .= "\n" . implode ( "\n", $subobjects ) . "\n" . $indent . "</" . $this->getTagName () . ">";
		} else {
			$tag1 .= " />";
		}
		return $tag1;
	}

	public function addElement($object) {
		$this->objects [] = $object;
	}

}

class HTML_SINGLE_ELEMENT extends HTML_ELEMENT {

	public function toString() {
		parent::toString ();
		$contents = $this->getContents ();
		$indent = $this->getIndentString ();
		$tag1 = $indent . "<" . $this->getTagName ();
		if (count ( $contents ) > 0) {
			$tag1 .= " " . implode ( " ", $contents );
		}
		$tag1 .= " />";
		return $tag1;
	}

}