<?php

class PreprocessorCatalog {
	private $contents;
	private $file;

	public function __construct($file) {
		$this->file = $file;
		$this->contents = array ();
		if (file_exists ( $file )) {
			$cat = file_get_contents ( $file );
			if ($cat) {
				$cat = json_decode ( $cat, true );
				if ($cat) {
					$this->contents = $cat;
				}
			}
		}
	}

	public function clear() {
		$this->contents = array ();
	}

	public function save() {
		file_put_contents ( $this->file, json_encode ( $this->contents ) );
	}

	public function checkAndStore($name, $value) {
		if (isset ( $this->contents [$name] )) {
			if ($this->contents [$name] == $value) {
				return true;
			} else {
				$this->contents [$name] = $value;
				return false;
			}
		} else {
			$this->contents [$name] = $value;
			return false;
		}
	}

}

abstract class Preprocessor {
	private $sourceFolder;
	private $destinationFolder;
	private $generatedPrefix;
	private $injections;
	private $catalog;
	private $includeAfter;

	public function addInjection($injection) {
		$this->injections [] = $injection;
	}

	public function commit() {
		if (! file_exists ( $this->destinationFolder )) {
			mkdir ( $this->destinationFolder );
		}
		$dir = scandir ( $this->sourceFolder );
		$files = array ();
		foreach ( $dir as $file ) {
			if ($file != "." && $file != ".." && ! is_dir ( $file )) {
				$files [] = $file;
			}
		}
		$catalog = $this->catalog;
		$injectionsChanged = false;
		foreach ( $this->injections as $injection ) {
			$metrics = $injection->getMetrics ();
			if (! $catalog->checkAndStore ( $injection->getFile (), $metrics )) {
				$injectionsChanged = true;
			}
		}
		if ($injectionsChanged) {
			$catalog->clear ();
			foreach ( $this->injections as $injection ) {
				$metrics = $injection->getMetrics ();
				$catalog->checkAndStore ( $injection->getFile (), $metrics );
			}
		}
		foreach ( $files as $file ) {
			$targetfile = $this->destinationFolder . "/" . $this->generatedPrefix . $file;
			$sourcefile = $this->sourceFolder . "/" . $file;
			$metrics = $this->getFileMetrics ( $sourcefile );
			if ((! $catalog->checkAndStore ( $sourcefile, $metrics )) || (! file_exists ( $targetfile ))) {
				$contents = file_get_contents ( $sourcefile );
				foreach ( $this->injections as $injection ) {
					$contents = $injection->apply ( $contents );
				}
				file_put_contents ( $targetfile, $contents );
			}
			if ($this->includeAfter) {
				include_once $targetfile;
			}
		}
		$catalog->save ();
	}

	public abstract function getFileMetrics($file);

	public function __construct($includeAfter, $sourceFolder, $destinationFolder, $generatedPrefix = "gen_", $injections = array(), $catalogName = "catalog.jsn") {
		$this->includeAfter = $includeAfter;
		$this->sourceFolder = $sourceFolder;
		$this->destinationFolder = $destinationFolder;
		$this->generatedPrefix = $generatedPrefix;
		$this->injections = $injections;
		$this->catalog = new PreprocessorCatalog ( $this->destinationFolder . "/" . $catalogName );
	}

	public static function handlePreprocessing() {
		$objects = array ();
		foreach ( get_declared_classes () as $class ) {
			if (is_subclass_of ( $class, get_called_class () )) {
				$objects [] = $class;
			}
		}
		foreach ( $objects as $object ) {
			$cls = new ReflectionClass ( $object );
			$instance = $cls->newInstance ( array () );
			$instance->commit ();
		}
	}

}