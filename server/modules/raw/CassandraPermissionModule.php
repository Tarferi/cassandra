<?php

class CassandraPermissionModule extends Module {
	
	public function getModuleData() {
		$body = Settings::getBody ();
		Settings::getTitle ()->setTitle ( "Cassandra module" );
		$body->addElement ( new HTML_CENTER ( new TitleBlockPanel ( "Cassandra Core", new NameText ( "This module is not supposed to be altered :)" ) ) ) );
		return true;
	}
	
	// @inject
}