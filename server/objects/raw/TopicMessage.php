<?php

class TopicMessage extends DatabaseObject {
	
	/**
	 * @ColumnType("int")
	 * @Primary
	 * @Getter
	 */
	private $id;
	
	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $senderID;
	
	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $topicID;
	
	/**
	 * @ColumnType("text")
	 * @Getter
	 */
	private $title;
	
	/**
	 * @ColumnType("text")
	 * @Getter
	 */
	private $contents;
	
	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $creationDate;

	
	// @inject
}
