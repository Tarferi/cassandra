<?php

abstract class Injection {
	private $func = array ();
	private $cls;
	private $setters;
	private $getters;
	private $file;

	public function __construct($expandSetters = true, $expandGetters = true) {
		$this->setters = $expandSetters;
		$this->getters = $expandGetters;
		$this->registerAnnotatedMethods ();
		$rc = new ReflectionClass ( $this );
		$this->file = $rc->getFileName ();
	}

	public function getFile() {
		return $this->file;
	}

	public function getMetrics() {
		return md5 ( file_get_contents ( $this->file ) );
	}

	public function apply($contents) {
		$result = array (
				$this->getInjections () 
		);
		if ($this->setters) {
			$result [] = $this->generateSetters ( $contents );
		}
		if ($this->getters) {
			$result [] = $this->generateGetters ( $contents );
		}
		$result [] = "\t// @inject"; // Repetitive injection
		$result = implode ( "\n", $result );
		$result = preg_replace ( "/^\s*\/\/\s*\@inject/mi", "\n" . $result, $contents );
		return $result;
	}

	private function generateGetters($contents) {
		$tokens = token_get_all ( $contents );
		$state = 0;
		$functions = array ();
		foreach ( $tokens as $token ) {
			$type = $token [0];
			if ($state == 0) {
				if ($type == T_DOC_COMMENT) {
					$value = $token [1];
					if (strpos ( $value, "@Getter" )) {
						$state = 1;
					}
				}
			} else if ($state == 1 && $type == T_VARIABLE) {
				$functions [] = $token [1];
				$state = 0;
			}
		}
		$ret = array ();
		foreach ( $functions as $variable ) {
			$varName = substr ( $variable, 1 );
			$code = "\n\tpublic function get" . ucfirst ( $varName ) . "() {\n\t\treturn \$this->" . $varName . ";\n\t}\n";
			$ret [] = $code;
		}
		return implode ( "\n", $ret );
	}

	private function generateSetters($contents) {
		$tokens = token_get_all ( $contents );
		$state = 0;
		$functions = array ();
		foreach ( $tokens as $token ) {
			$type = $token [0];
			if ($state == 0) {
				if ($type == T_DOC_COMMENT) {
					$value = $token [1];
					if (strpos ( $value, "@Setter" )) {
						$state = 1;
					}
				}
			} else if ($state == 1 && $type == T_VARIABLE) {
				$functions [] = $token [1];
				$state = 0;
			}
		}
		$ret = array ();
		foreach ( $functions as $variable ) {
			$varName = substr ( $variable, 1 );
			$code = "\n\tpublic function set" . ucfirst ( $varName ) . "(\$" . $varName . ") {\n\t\t\$this->" . $varName . "=\$" . $varName . ";\n\t}\n";
			$ret [] = $code;
		}
		return implode ( "\n", $ret );
	}

	protected function registerAnnotatedMethods() {
		$cls = new ReflectionAnnotatedClass ( get_class ( $this ) );
		$methods = $cls->getMethods ();
		foreach ( $methods as $method ) {
			$inject = $method->getAnnotation ( "Inject" ) !== false;
			if ($inject) {
				$this->registerFunction ( $method->getName () );
			}
		}
	}

	protected function registerFunction($func) {
		$this->cls = new ReflectionClass ( get_class ( $this ) );
		$this->func [$func] = $this->cls->getMethod ( $func );
	}

	private function getInjections() {
		$this->cls = new ReflectionClass ( get_class ( $this ) );
		$injections = array ();
		$files = array ();
		foreach ( $this->func as $fnc ) {
			$injections [] = $this->getFunction ( $fnc );
		}
		return implode ( "\n", $injections );
	}

	private function getFunction($func) {
		static $contentArray = array ();
		$filename = $func->getFileName ();
		if (! isset ( $contentArray [$filename] )) {
			$contentArray [$filename] = file ( $filename );
		}
		$source = $contentArray [$filename];
		$start_line = $func->getStartLine () - 1; // it's actually - 1, otherwise you wont get the function() block
		$end_line = $func->getEndLine ();
		$length = $end_line - $start_line;
		return implode ( "", array_slice ( $source, $start_line, $length ) );
	}

}