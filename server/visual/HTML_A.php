<?php

class HTML_A extends HTML_PAIR_ELEMENT {
	private $title;
	private $target;
	private $link;

	public function toString() {
		static $added = false;
		if (! $added) {
			$added = true;
			$this->addAttribute ( "target", $this->target );
			if ($this->link !== false) {
				$this->addAttribute ( "href", $this->link );
			}
		}
		return parent::toString ();
	}

	public function __construct($title = false, $link = false, $target = "__blank") {
		parent::__construct ( "a" );
		$this->title = new HTML_TEXT ( $title === false ? "" : $title );
		$this->addElement ( $this->title );
		$this->link = $link;
		$this->target = $target;
	}

	public function setTarget($target) {
		$this->target = $target;
	}

	public function setLink($link) {
		$this->link = $link;
	}

	public function setText($text) {
		$this->title->setText ( $text );
	}

}