<?php

class HTML_TD extends HTML_PAIR_ELEMENT {

	public function __construct() {
		parent::__construct ( "td" );
	}

	public function setValign($valign) {
		$this->getRepresentationObject ()->addAttribute ( "valign", $valign );
	}
	public function setAlign($align) {
		$this->getRepresentationObject ()->addAttribute ( "align", $align );
	}
	
}