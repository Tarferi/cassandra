<?php
class HTML_CENTER extends HTML_PAIR_ELEMENT {

	public function __construct($wrap=false) {
		parent::__construct ( "center" );
		if($wrap !== false) {
			$this->addElement($wrap);
		}
	}

}