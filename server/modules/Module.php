<?php

abstract class Module extends TitlePanel {
	private $name;
	private $permission;

	public function __construct() {
		$this->name = get_class($this);
		$this->permission = static::getPermissionName ();
	}
	
	public function getPermission() {
		return $this->permission;
	}

	public function getName() {
		return $this->name;
	}

	public function getModuleData() {
		return false;
	}
}