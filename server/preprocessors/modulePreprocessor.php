<?php

class ModulePreprocessor extends Preprocessor {

	public function __construct() {
		parent::__construct ( true, "modules/raw", "modules/processed" );
		$this->addInjection ( new ModuleInjection () );
	}

	public function getFileMetrics($file) {
		$val = md5 ( file_get_contents ( $file ) );
		return $val;
	}

}