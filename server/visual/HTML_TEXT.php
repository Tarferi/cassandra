<?php

class HTML_TEXT extends HTML_PAIR_ELEMENT {
	private $text;

	public function toString() {
		$indent = $this->getIndentString ();
		return $indent . str_replace ( "\n", "<br />", htmlspecialchars ( $this->text ) );
	}

	public function setText($text) {
		$this->text = $text;
	}

	public function getText() {
		return $this->text;
	}

	public function __construct($text = "") {
		$this->text = $text;
	}

}