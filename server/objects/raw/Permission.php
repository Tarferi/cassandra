<?php
class Permission extends DatabaseObject {
	/**
	 * @ColumnType("int")
	 * @Primary
	 * @Getter
	 */
	private $id;
	
	/**
	 * @ColumnType("int")
	 * @Getter
	 */
	private $ownerID;
	
	/**
	 * @ColumnType("int");
	 * @Getter
	 */
	private $targetID;
	
	/**
	 * @ColumnType("text")
	 * @Getter
	 */
	private $targetType;
	
	/**
	 * @ColumnType("int");
	 * @Getter
	 * @Setter
	 */
	private $level;
	

	/**
	 * @ColumnType("int")
	 * @Getter
	 * @Setter
	 */
	private $creationDate;

	// @inject
}