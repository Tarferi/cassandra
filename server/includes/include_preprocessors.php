<?php
include_once "data/preprocessor.php";
include_once "data/injection.php";


include "objects/injections/DatabaseObjectsInjection.php";
include_once "preprocessors/objectPreprocessor.php";
include "modules/injections/ModuleInjection.php";
include_once "preprocessors/modulePreprocessor.php";

include_once "preprocessors/databasePreprocessor.php";
