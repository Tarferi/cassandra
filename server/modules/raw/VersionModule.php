<?php

class VersionModule extends Module {
	
	/**
	 * @Getter
	 */
	private $currentVersionHash;

	public function getModuleData() {
		$doc = new TitleBlockPanel ( "Version module" );
		$doc->addComponent ( new HTML_TEXT ( "This is just a test" ) );
		Settings::getBody()->addElement(new HTML_CENTER($doc));
		return new HTML_CENTER ( $doc );
	}
	
	// @inject
}