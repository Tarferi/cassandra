<?php

class ErrorText extends HTML_SPAN {
	private $content;

	public function __construct($text) {
		parent::__construct ();
		$this->construct ($text);
	}

	public function setText($text) {
		$this->content->setText ( $text );
	}

	private function construct($text) {
		$this->content = new HTML_TEXT ($text);
		$this->addElement ( $this->content );
		$this->setFontFamily ( "Verdana" );
		$this->setFontSize ( "12" );
		$this->setForeground ( "#ff0000" );
	}

}