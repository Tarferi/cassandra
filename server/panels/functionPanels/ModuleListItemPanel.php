<?php

class ModuleListItemPanel extends HTML_DIV {

	public function __construct($moduleListItem) {
		parent::__construct ();
		$this->construct ( $moduleListItem );
		$this->setName ( $moduleListItem->getClassName () );
	}

	private function construct($module) {
		$tp = new TitleBlockPanel ( "Module" );
		$block = new tablePanel ();
		$tp->addComponent ( $block );
		$block->addComponent ( new NameText ( "Module name:" ), 0, 0 );
		$block->addComponent ( new NameCode ( $module->getClassName () ), 0, 1 );
		$block->addComponent ( new NameText ( "Module permission:" ), 1, 0 );
		$block->addComponent ( new NameCode ( $module->getPermission () ), 1, 1 );
		$block->getColumnAt ( 0, 0 )->setAlign ( "right" );
		$block->getColumnAt ( 1, 0 )->setAlign ( "right" );
		
		$tp->setWidth ( "100%" );
		$tp->setDisplay ( "" );
		
		$this->addElement ( $tp );
		$this->setWidth ( "100%" );
		$this->setDisplay ( "inline-block" );
	}

}